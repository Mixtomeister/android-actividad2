package thinkback.mixtomeister.lib.eventadmin

import android.view.View
import java.util.function.Consumer


class EventAdmin: View.OnClickListener{
    lateinit var listener: EventAdminListener
    lateinit var CLICK: String

    init {
        CLICK = "click"
    }

    override fun onClick(view: View) {
        this.listener.notifyEvent(CLICK, view)
    }
}