package thinkback.mixtomeister.lib.eventadmin

import android.view.View

/**
 * Created by Mixtomeister on 26/11/2017.
 */
interface EventAdminListener {
    fun notifyEvent(event: String, view: View)
}