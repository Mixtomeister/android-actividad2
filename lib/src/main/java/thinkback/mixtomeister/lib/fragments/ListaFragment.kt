package thinkback.mixtomeister.lib.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import thinkback.mixtomeister.lib.R

class ListaFragment : Fragment() {

    lateinit var lista: RecyclerView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_list, container, false)
        lista = view.findViewById(R.id.view_list)
        lista.layoutManager = GridLayoutManager(context, 1)
        return view
    }
}
