package thinkback.mixtomeister.actividad2

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import thinkback.mixtomeister.actividad2.Firebase.FirebaseAdmin
import thinkback.mixtomeister.actividad2.Firebase.FirebaseLoginAdminListener
import thinkback.mixtomeister.lib.eventadmin.EventAdminListener
import thinkback.mixtomeister.lib.fragments.LoginFragment
import thinkback.mixtomeister.lib.fragments.LoginFragmentListener
import thinkback.mixtomeister.lib.fragments.RegisterFragment
import thinkback.mixtomeister.lib.fragments.RegisterFragmentListener



class LoginActivity : AppCompatActivity(), FirebaseLoginAdminListener, RegisterFragmentListener, LoginFragmentListener {
    lateinit var fragLogin: LoginFragment
    lateinit var fragRegister: RegisterFragment

    lateinit var container: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        FirebaseAdmin.listenerLogin = this

        fragLogin = LoginFragment()
        fragRegister = RegisterFragment()

        container = findViewById(R.id.fragmentContainer)

        fragRegister.listener = this

        showLogin()
    }

    fun showLogin(){
        var tran = supportFragmentManager.beginTransaction()
        tran.add(container.id, fragLogin)
        tran.commit()
    }

    fun showRegister(){
        var tran = supportFragmentManager.beginTransaction()
        tran.hide(fragLogin)
        tran.show(fragRegister)
        tran.commit()
    }

    fun clearLogin(){
        fragLogin.txtEmail.setText("")
        fragLogin.txtPass.setText("")
    }

    fun clearRegister(){
        fragRegister.txtNick.setText("")
        fragRegister.txtEmail.setText("")
        fragRegister.txtEmailS.setText("")
        fragRegister.txtPass.setText("")
        fragRegister.txtPassS.setText("")
    }

    override fun onLoginDone() {
        FirebaseAdmin.login(fragLogin.txtEmail.text.toString(), fragLogin.txtPass.text.toString())
    }

    override fun onRegisterBtn() {
        showRegister()
    }

    override fun onRegisterDone() {
        FirebaseAdmin.crearUsuario(fragRegister.txtEmail.text.toString(), fragRegister.txtPass.text.toString())
    }

    override fun onEmptyFields() {
        Toast.makeText(this, "Los campos no pueden estar vacios", Toast.LENGTH_SHORT).show()
    }

    override fun onFieldsNotEquals() {
        Toast.makeText(this, "Los campos no coinciden", Toast.LENGTH_SHORT).show()
    }

    override fun onBackBtn() {
        showLogin()
    }

    override fun FIRcomplete(task: String) {
        if(task == FirebaseAdmin.LOGIN){
            Toast.makeText(this, "Login complete", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }else if(task == FirebaseAdmin.REGISTER){
            Toast.makeText(this, "Usuario creado", Toast.LENGTH_SHORT).show()
            showLogin()
            clearLogin()
            fragLogin.txtEmail.setText(fragRegister.txtEmail.text.toString())
            clearRegister()
        }
    }

    override fun FIRfail(task: String, err: String) {
        if(task == FirebaseAdmin.LOGIN){
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show()
            clearLogin()
        }else if(task == FirebaseAdmin.REGISTER){
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show()
        }
    }
}
