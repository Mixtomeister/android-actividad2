package thinkback.mixtomeister.actividad2.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import thinkback.mixtomeister.actividad2.Firebase.Tesla
import thinkback.mixtomeister.actividad2.R

/**
 * Created by Mixtomeister on 18/12/2017.
 */
class ListaTeslasAdapter(var array: ArrayList<Tesla>): RecyclerView.Adapter<TeslaViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TeslaViewHolder {
        return TeslaViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.cell_tesla_layout, null))
    }

    override fun onBindViewHolder(holder: TeslaViewHolder?, position: Int) {
        holder!!.lblModel.setText(array[position].model)
    }

    override fun getItemCount(): Int {
        return array.size
    }
}

class TeslaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    lateinit var lblModel: TextView

    init {
        lblModel = itemView.findViewById(R.id.lblModel)
    }
}