package thinkback.mixtomeister.actividad2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.GenericTypeIndicator
import thinkback.mixtomeister.actividad2.Firebase.FirebaseAdmin
import thinkback.mixtomeister.actividad2.Firebase.FirebaseDatabaseListener
import thinkback.mixtomeister.actividad2.Firebase.Tesla
import thinkback.mixtomeister.actividad2.adapters.ListaTeslasAdapter
import thinkback.mixtomeister.lib.fragments.ListaFragment

class MainActivity : AppCompatActivity(), FirebaseDatabaseListener {

    lateinit var listFragment: ListaFragment

    lateinit var array: ArrayList<Tesla>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listFragment = supportFragmentManager.findFragmentById(R.id.list_fragment) as ListaFragment
        listFragment.lista.adapter = ListaTeslasAdapter(ArrayList<Tesla>())
        FirebaseAdmin.listenerDatabase = this
        FirebaseAdmin.escucharRama("teslas")
    }

    override fun ramaDescargada(data: DataSnapshot) {
        val typeIndicator = object : GenericTypeIndicator<ArrayList<Tesla>>() {}
        this.array = data.getValue(typeIndicator)!!
        listFragment.lista.adapter = ListaTeslasAdapter(this.array!!)
    }
}
