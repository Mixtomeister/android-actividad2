package thinkback.mixtomeister.actividad2.Firebase

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

object FirebaseAdmin{
    var auth: FirebaseAuth
    var database: DatabaseReference
    lateinit var listenerLogin: FirebaseLoginAdminListener
    lateinit var listenerDatabase: FirebaseDatabaseListener

    var LOGIN: String
    var REGISTER: String

    init {
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference
        LOGIN = "login"
        REGISTER = "register"
    }

    fun login(email: String, pass: String){
        auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener{ task: Task<AuthResult> ->
            if (task.isSuccessful){
                listenerLogin.FIRcomplete(LOGIN)
            }else{
                listenerLogin.FIRfail(LOGIN, "La autentificación ha fallado!")
            }
        }
    }

    fun crearUsuario(email: String, pass: String){
        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener{ task: Task<AuthResult> ->
            if (task.isSuccessful){
                listenerLogin.FIRcomplete(REGISTER)
            }else{
                listenerLogin.FIRfail(REGISTER, "El registro ha fallado imbecil!")
            }
        }
    }

    fun escucharRama(rama: String){
        database.child(rama).addValueEventListener(object : ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                listenerDatabase.ramaDescargada(dataSnapshot)
            }

            override fun onCancelled(p0: DatabaseError?) {}
        })
    }
}