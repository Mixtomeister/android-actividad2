package thinkback.mixtomeister.actividad2.Firebase

import com.google.firebase.database.DataSnapshot

/**
 * Created by Mixtomeister on 18/12/2017.
 */
interface FirebaseDatabaseListener {
    fun ramaDescargada(data: DataSnapshot)
}