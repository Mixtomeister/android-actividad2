package thinkback.mixtomeister.actividad2.Firebase

import com.google.firebase.database.IgnoreExtraProperties

/**
 * Created by Mixtomeister on 18/12/2017.
 */
@IgnoreExtraProperties
class Tesla {
    lateinit var model: String

    constructor(model : String){
        this.model = model
    }

    constructor(){}
}