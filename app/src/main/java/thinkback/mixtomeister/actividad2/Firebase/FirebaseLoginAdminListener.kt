package thinkback.mixtomeister.actividad2.Firebase

import com.google.firebase.database.DataSnapshot

/**
 * Created by Mixtomeister on 26/11/2017.
 */
interface FirebaseLoginAdminListener {
    fun FIRcomplete(task: String)
    fun FIRfail(task: String, err: String)
}